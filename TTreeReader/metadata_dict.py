# Metadata files

# jRoundJets

# Dijets uncalibrated
#Dijets         = dict()
#Dijets[361021] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361021_170920_cutflow.root/361021_cutflow.root"
#Dijets[361022] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361022_170920_cutflow.root/361022_cutflow.root"
#Dijets[361023] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361023_170920_cutflow.root/361023_cutflow.root"
#Dijets[361024] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Uncalibrated/user.jbossios.jbossios.Dijets_361024_170920_cutflow.root/361024_cutflow.root"
# Dijets calibrated
#DijetsCalibrated         = dict()
#DijetsCalibrated[361021] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361021_jFEXcalibrated_220920_cutflow.root/361021_cutflow.root"
#DijetsCalibrated[361022] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361022_jFEXcalibrated_220920_cutflow.root/361022_cutflow.root"
#DijetsCalibrated[361023] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361023_jFEXcalibrated_220920_cutflow.root/361023_cutflow.root"
#DijetsCalibrated[361024] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_Sep20/Calibrated/user.jbossios.jbossios.Dijets_361024_jFEXcalibrated_220920_cutflow.root/361024_cutflow.root"

# jRoundJetsPUsub

# Dijets uncalibrated
Dijets         = dict()
# Produced with old AODs already deleted
#Dijets[361020] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Dec20/Uncalibrated/user.jbossios.jbossios.Dijets_361020jRoundJetsPUsub_081220_cutflow.root/361020_cutflow.root"
#Dijets[361021] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Dec20/Uncalibrated/user.jbossios.jbossios.Dijets_361021jRoundJetsPUsub_081220_cutflow.root/361021_cutflow.root"
#Dijets[361022] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Dec20/Uncalibrated/user.jbossios.jbossios.Dijets_361022jRoundJetsPUsub_081220_cutflow.root/361022_cutflow.root"
#Dijets[361023] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Dec20/Uncalibrated/user.jbossios.jbossios.Dijets_361023jRoundJetsPUsub_081220_cutflow.root/361023_cutflow.root"
#Dijets[361024] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Dec20/Uncalibrated/user.jbossios.jbossios.Dijets_361024jRoundJetsPUsub_081220_cutflow.root/361024_cutflow.root"
# TTrees produced with Jan 2021 AODs
#Dijets[361020] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Jan21/Uncalibrated/user.jbossios.jbossios.Dijets_361020jRoundJetsPUsub_newAODs_080121_cutflow.root/361020_cutflow.root"
#Dijets[361021] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Jan21/Uncalibrated/user.jbossios.jbossios.Dijets_361021jRoundJetsPUsub_newAODs_080121_cutflow.root/361021_cutflow.root"
#Dijets[361022] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Jan21/Uncalibrated/user.jbossios.jbossios.Dijets_361022jRoundJetsPUsub_newAODs_080121_cutflow.root/361022_cutflow.root"
#Dijets[361023] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Jan21/Uncalibrated/user.jbossios.jbossios.Dijets_361023jRoundJetsPUsub_newAODs_080121_cutflow.root/361023_cutflow.root"
#Dijets[361024] = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Jan21/Uncalibrated/user.jbossios.jbossios.Dijets_361024jRoundJetsPUsub_newAODs_080121_cutflow.root/361024_cutflow.root"
# TTrees produced with Ben's AODs with lower L1 thresholds
Dijets[361020]="/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Mar21/Uncalibrated/user.jbossios.bcarlson.Dijets_361020jRoundJetsPUsub_lowerL1Thresholds_020321_cutflow.root/361020_cutflow.root"
Dijets[361021]="/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Mar21/Uncalibrated/user.jbossios.bcarlson.Dijets_361021jRoundJetsPUsub_lowerL1Thresholds_020321_cutflow.root/361021_cutflow.root"
Dijets[361022]="/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Mar21/Uncalibrated/user.jbossios.bcarlson.Dijets_361022jRoundJetsPUsub_lowerL1Thresholds_020321_cutflow.root/361022_cutflow.root"
Dijets[361023]="/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Mar21/Uncalibrated/user.jbossios.bcarlson.Dijets_361023jRoundJetsPUsub_lowerL1Thresholds_020321_cutflow.root/361023_cutflow.root"
Dijets[361024]="/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Mar21/Uncalibrated/user.jbossios.bcarlson.Dijets_361024jRoundJetsPUsub_lowerL1Thresholds_020321_cutflow.root/361024_cutflow.root"
# Dijets calibrated (w/o interpolation)
#DijetsCalibrated = dict()
#PATH             = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Jan21/Calibrated/"
#DijetsCalibrated[361020] = PATH+"user.jbossios.jbossios.Dijets_361020jRoundJetsPUsub_calibrated_180121_cutflow.root/361020_cutflow.root"
#DijetsCalibrated[361021] = PATH+"user.jbossios.jbossios.Dijets_361021jRoundJetsPUsub_calibrated_180121_cutflow.root/361021_cutflow.root"
#DijetsCalibrated[361022] = PATH+"user.jbossios.jbossios.Dijets_361022jRoundJetsPUsub_calibrated_180121_cutflow.root/361022_cutflow.root"
#DijetsCalibrated[361023] = PATH+"user.jbossios.jbossios.Dijets_361023jRoundJetsPUsub_calibrated_180121_cutflow.root/361023_cutflow.root"
#DijetsCalibrated[361024] = PATH+"user.jbossios.jbossios.Dijets_361024jRoundJetsPUsub_calibrated_180121_cutflow.root/361024_cutflow.root"
# Dijets calibrated (with interpolation)
DijetsCalibrated = dict()
#PATH             = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_CalibratedWithInterpolation_Feb21/Calibrated_Interpolating/"
#DijetsCalibrated[361020] = PATH+"user.jbossios.jbossios.Dijets_361020jRoundJetsPUsub_calibrated_interpolating_180221_cutflow.root/361020_cutflow.root"
#DijetsCalibrated[361021] = PATH+"user.jbossios.jbossios.Dijets_361021jRoundJetsPUsub_calibrated_interpolating_180221_cutflow.root/361021_cutflow.root"
#DijetsCalibrated[361022] = PATH+"user.jbossios.jbossios.Dijets_361022jRoundJetsPUsub_calibrated_interpolating_180221_cutflow.root/361022_cutflow.root"
#DijetsCalibrated[361023] = PATH+"user.jbossios.jbossios.Dijets_361023jRoundJetsPUsub_calibrated_interpolating_180221_cutflow.root/361023_cutflow.root"
#DijetsCalibrated[361024] = PATH+"user.jbossios.jbossios.Dijets_361024jRoundJetsPUsub_calibrated_interpolating_180221_cutflow.root/361024_cutflow.root"
# Dijets calibrated (lower thresholds and no interpolation)
PATH             = "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_jFEX_jRoundJetsPUsub_Mar21/Calibrated/"
DijetsCalibrated[361020] = PATH+"user.jbossios.bcarlson.Dijets_361020jRoundJetsPUsub_lowerL1Thresholds_calibrated_040321_cutflow.root/361020_cutflow.root"
DijetsCalibrated[361021] = PATH+"user.jbossios.bcarlson.Dijets_361021jRoundJetsPUsub_lowerL1Thresholds_calibrated_040321_cutflow.root/361021_cutflow.root"
DijetsCalibrated[361022] = PATH+"user.jbossios.bcarlson.Dijets_361022jRoundJetsPUsub_lowerL1Thresholds_calibrated_040321_cutflow.root/361022_cutflow.root"
DijetsCalibrated[361023] = PATH+"user.jbossios.bcarlson.Dijets_361023jRoundJetsPUsub_lowerL1Thresholds_calibrated_040321_cutflow.root/361023_cutflow.root"
DijetsCalibrated[361024] = PATH+"user.jbossios.bcarlson.Dijets_361024jRoundJetsPUsub_lowerL1Thresholds_calibrated_040321_cutflow.root/361024_cutflow.root"

MetadataFiles = dict()
MetadataFiles["Dijets"]           = Dijets
MetadataFiles["DijetsCalibrated"] = DijetsCalibrated

