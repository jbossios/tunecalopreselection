# Inputs
InputFiles = [ # path to ROOT files
# new calibration
#  "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_preselStudies/latestEMcalib/ttbarwoPileup/user.jbossios.410000.ttbarwoPileupMayRefPreselectionStudies_210521_tree.root/", # wo pileup
#  "/eos/atlas/atlascerngroupdisk/trig-jet/NTUP_preselStudies/latestEMcalib/ttbarwPileup/user.jbossios.410000.ttbarwPileupMayRefPreselectionStudies_210521_tree.root/", # w pileup
]
Dataset   = "ttbarwPileup" # options: ttbarwoPileup and ttbarwPileup

# Debugging
Debug     = False

###################################################
## DO NOT MODIFY
###################################################

from ROOT import *
import os,sys
from Arrays import *

os.system('mkdir Testing')

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)

# Run over a single file from the chosen dataset
counter = 0
command = ""
for File in os.listdir(InputFiles[0]): # Loop over files
  if ".root" not in File:
    continue
  if counter > 0:
    break

  # Run test job
  command += "python Reader.py --path "+InputFiles[0]+" --outPATH Testing/ --file "+File+" --dataset "+Dataset
  if Debug:
    command += " --debug"
  command += " && "
  counter += 1

command = command[:-2]
os.system(command)
