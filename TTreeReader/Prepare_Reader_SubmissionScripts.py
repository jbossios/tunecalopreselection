Datasets = ["Dijets"] 
#Datasets = ["DijetsCalibrated"] 

# Location where the output ROOT files will be written
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/170920/" # L1pt > 7 GeV
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/180920/" # no L1 pt cut
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/220920/"  # L1 jets calibrated
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/07102020_notCalibrated/" # new not calibrated
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/07102020_calibrated/" # new calibrated
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_15122020/" # jFEX jRoundJetsPUsub not calibrated
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_14012021/" # jFEX jRoundJetsPUsub not calibrated (using Jan 2021 TTrees)
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_21012021/" # jFEX jRoundJetsPUsub calibrated (using Jan 2021 TTrees)
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_interpolating_22022021/" # jFEX jRoundJetsPUsub calibrated using interpolation (using Feb 2021 TTrees)
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_03032021/" # jFEX jRoundJetsPUsub not calibrated using new TTrees with lower L1 thresholds
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_Calibrated_04032021/" # jFEX jRoundJetsPUsub calibrated using new TTrees with lower L1 thresholds
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_finerEtaBins_09032021/" # jFEX jRoundJetsPUsub not calibrated AND lower L1 thresholds AND finer eta bins
#outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_fixedCalib_calibrated_12032021/" # jFEX jRoundJetsPUsub calibrated AND lower L1 thresholds AND fixed calib
outPATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_calibrated_16032021/" # jFEX jRoundJetsPUsub calibrated AND lower L1 thresholds AND fixed calib v2

# Calibrate?
Calibrate = True
CalibFile = '/eos/atlas/atlascerngroupdisk/trig-jet/Jona/jFEXcalibrations/L1JetCalibratorInputs/jRoundJetsPUsub/TH2DResponse_vs_L1pt_eta_jRoundJetsPUsub_lowerThresholds_03032021.root'

# Debugging
Debug   = False

###################################################
## DO NOT MODIFY
###################################################

print "<<< Creating submission scripts <<<"

from ROOT import *
import os,sys

os.system("rm SubmissionScripts/*")
os.system("mkdir SubmissionScripts")

from InputLists import Inputs
from HelperFunctions import *
from Arrays import *

# Loop over datasets
for Dataset in Datasets:

  # Protections
  if Dataset not in DatasetOptions:
    print "ERROR: Dataset not recognised, exiting"
    sys.exit(0)
  if not Inputs.has_key(Dataset):
    print "ERROR: There are no inputs yet for the dataset provided, exiting"
    sys.exit(0)

  # Select input files for the given dataset
  InputFiles = Inputs[Dataset]

  ###########################
  # Loop over input files
  ###########################
  for path in InputFiles: # Loop over paths

    DSID = getDSID(path)

    for File in os.listdir(path): # Loop over files

      if ".root" not in File:
        continue

      # Check if TTree exists
      tfile = TFile.Open(path+'/'+File)
      Dir   = tfile.Get("TreeAlgo")
      if not Dir:
        print('ERROR: TreeAlgo directory not found, exiting')
	sys.exit(0)
      tree  = Dir.Get("nominal")
      if not tree:
        print "Skipping "+path+'/'+File+' with no TTree'
        continue # file with no TTree

      # Create submission script
      ExtraArgs   = ""
      ScriptName  = Dataset
      ScriptName += "_" + DSID
      ScriptName += "_" + File
      outputFile = open("SubmissionScripts/"+ScriptName+".sub","w")
      outputFile.write("executable = ../SubmissionScripts/"+ScriptName+".sh\n")
      outputFile.write("input      = FilesForSubmission.tar.gz\n")
      outputFile.write("output     = Logs/"+ScriptName+".$(ClusterId).$(ProcId).out\n")
      outputFile.write("error      = Logs/"+ScriptName+".$(ClusterId).$(ProcId).err\n")
      outputFile.write("log        = Logs/"+ScriptName+".$(ClusterId).log\n")
      #outputFile.write("RequestMemory   = 6000\n")
      #outputFile.write("RequestCpus = 4\n")
      outputFile.write("transfer_output_files = \"\" \n")
      #outputFile.write('+JobFlavour = "tomorrow"\n')
      #outputFile.write('+JobFlavour = "testmatch"\n')
      #outputFile.write('+JobFlavour = "espresso"\n')
      #outputFile.write('+JobFlavour = "microcentury"\n')
      #outputFile.write('+JobFlavour = "longlunch"\n')
      outputFile.write('+JobFlavour = "workday"\n')
      outputFile.write("arguments  = $(ClusterId) $(ProcId)\n")
      outputFile.write("queue")
      outputFile.close()

      # Create bash script which will run the Reader
      outputFile = open("SubmissionScripts/"+ScriptName+".sh","w")
      outputFile.write("#!/bin/bash\n")
      outputFile.write("tar xvzf FilesForSubmission.tar.gz\n")
      outputFile.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n") # for setupATLAS
      outputFile.write("source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n") # setupATLAS
      outputFile.write("lsetup 'root 6.20.02-x86_64-centos7-gcc8-opt'\n")
      outputFile.write("python Reader.py --path "+path+" --outPATH "+outPATH+" --file "+File+" --dataset "+Dataset)
      if Calibrate:
        outputFile.write(" --calibrate --calibFile {}".format(CalibFile))
      outputFile.close()

print "<<< All DONE <<<"
