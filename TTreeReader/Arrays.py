##################################
# Expected datasets
##################################
DatasetOptions = ['ttbarwoPileup','ttbarwPileup']

# Eta binning
EtaBins  = [-4.5+x*0.5 for x in xrange(0,19)]
nEtaBins = len(EtaBins)-1
#EtaBins  = [-4.5+x*0.1 for x in xrange(0,91)]
#nEtaBins = len(EtaBins)-1
