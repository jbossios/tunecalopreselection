
#####################
# EM JETS
#####################
def getNEMJet(Branches):
  return len(Branches["trigEM_pt"])

def getEMJetPt(Branches,i):
  return Branches["trigEM_pt"][i]

def getEMJetPhi(Branches,i):
  return Branches["trigEM_phi"][i]

def getEMJetEta(Branches,i):
  return Branches["trigEM_eta"][i]

def getEMJetE(Branches,i):
  return Branches["trigEM_E"][i]


#####################
# PF JETS
#####################
def getNPFJet(Branches):
  return len(Branches["trigPF_pt"])

def getPFJetPt(Branches,i):
  return Branches["trigPF_pt"][i]

def getPFJetPhi(Branches,i):
  return Branches["trigPF_phi"][i]

def getPFJetEta(Branches,i):
  return Branches["trigPF_eta"][i]

def getPFJetE(Branches,i):
  return Branches["trigPF_E"][i]

#####################
# Other
#####################
def getDSID(path):
  if path.find("4100") != -1:
    return path[path.find("4100"):path.find("4100")+6]


# SetBranchStatus
def setBranchStatus(tree):
  tree.SetBranchStatus("*",0) # disable all branches
  tree.SetBranchStatus("eventNumber",1)
  tree.SetBranchStatus("mcEventWeight",1)
  tree.SetBranchStatus("mcChannelNumber",1)
  tree.SetBranchStatus("trigEM_pt",1)
  tree.SetBranchStatus("trigEM_phi",1)
  tree.SetBranchStatus("trigEM_eta",1)
  tree.SetBranchStatus("trigEM_E",1)
  tree.SetBranchStatus("trigPF_pt",1)
  tree.SetBranchStatus("trigPF_eta",1)
  tree.SetBranchStatus("trigPF_phi",1)
  tree.SetBranchStatus("trigPF_E",1)


# SetBranchAddress
import ROOT,array
def setBranchAddress(tree, Branches):
  Branches["eventNumber"] = array.array('i',[0])
  tree.SetBranchAddress("eventNumber",Branches["eventNumber"])
  Branches["mcEventWeight"] = array.array('f',[0])
  tree.SetBranchAddress("mcEventWeight",Branches["mcEventWeight"])
  Branches["mcChannelNumber"] = array.array('i',[0])
  tree.SetBranchAddress("mcChannelNumber",Branches["mcChannelNumber"])
  Branches["trigEM_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigEM_pt",Branches["trigEM_pt"])
  Branches["trigEM_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigEM_eta",Branches["trigEM_eta"])
  Branches["trigEM_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigEM_phi",Branches["trigEM_phi"])
  Branches["trigEM_E"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigEM_E",Branches["trigEM_E"])
  Branches["trigPF_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigPF_pt",Branches["trigPF_pt"])
  Branches["trigPF_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigPF_eta",Branches["trigPF_eta"])
  Branches["trigPF_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigPF_phi",Branches["trigPF_phi"])
  Branches["trigPF_E"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("trigPF_E",Branches["trigPF_E"])
