
Datasets = ["Dijets"]
#Datasets = ["DijetsCalibrated"]

# Path to condor outputs
#Date = "07102020_notCalibrated"
#Date = "07102020_calibrated"
#Date = "jRoundJetsPUsub_15122020" # uncalibrated
#Date = "jRoundJetsPUsub_14012021" # uncalibrated
#Date = "jRoundJetsPUsub_21012021" # calibrated (not interpolating)
#Date = "jRoundJetsPUsub_interpolating_22022021" # calibrated (interpolating)
#Date = "jRoundJetsPUsub_lowerThresholds_03032021"
#Date = "jRoundJetsPUsub_lowerThresholds_Calibrated_04032021"
#Date = 'jRoundJetsPUsub_lowerThresholds_finerEtaBins_09032021'
#Date = 'jRoundJetsPUsub_lowerThresholds_fixedCalib_calibrated_12032021'
Date = 'jRoundJetsPUsub_lowerThresholds_calibrated_16032021'
PATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/"+Date+"/"

Calibrated = True

######################################################################
## DO NOT MODIFY
######################################################################

import os,sys

os.system('mkdir ../../Fitter/Inputs')

command = ""

for Dataset in Datasets:
  command  = "hadd ../../Fitter/Inputs/"
  command += Dataset
  if Calibrated and 'Calibrated' not in Dataset:
    command += 'Calibrated'
  command += "_All_"+Date+".root "+PATH
  command += Dataset+"*"
  command += ".root"
  os.system(command)
