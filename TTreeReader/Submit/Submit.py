####################################################
#                                                  #
# Author: Jona Bossio (jbossios@cern.ch)           #
# Date:   30 November 2019                         #
#                                                  #
####################################################

Dataset = "Dijets"
#Dataset = "DijetsCalibrated"

# output path
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/07102020_notCalibrated/'
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/07102020_calibrated/'
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_15122020/'
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_14012021/'
#PATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_21012021/"
#PATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_interpolating_22022021/"
#PATH = "/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_03032021/"
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_Calibrated_04032021/'
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_finerEtaBins_09032021/'
#PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_fixedCalib_calibrated_12032021/'
PATH = '/eos/atlas/atlascerngroupdisk/trig-jet/HIST_jFEX/jRoundJetsPUsub_lowerThresholds_calibrated_16032021/'

Test = True

######################################################################
## DO NOT MODIFY
######################################################################

import os,sys

os.system('mkdir Logs')

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../')
from Arrays import *

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)

# Find all the local Reader's outputs
ROOTfiles = []
AllFiles = os.listdir(PATH)
for File in AllFiles:
  if ".root" in File:
    ROOTfiles.append(File)

counter      = 0
path         = "../SubmissionScripts/"
localCommand = ""
for File in os.listdir(path): # Loop over submission scripts files
  if ".sub" not in File:
    continue
  if Dataset not in File:
    continue

  # Check if there is an output already for this job
  ROOTfileFound = False
  FileName      = File.replace("Selection","")
  FileName      = FileName.replace(".sub","")
  for rootFile in ROOTfiles: # look at reader's outputs
    if FileName in rootFile: # there is already an output for this submission script
      ROOTfileFound = True
      break
  if ROOTfileFound:
    continue
  counter += 1
  command = "condor_submit "+path+File+" &"
  if not Test: os.system(command)
  else: # Print commands to run locally
    FileName = path+File.replace(".sub",".sh")
    File     = open(FileName,"r")
    for line in File:
      if "python" in line: localCommand += line + " && "
if Test: print localCommand[:-2]
if counter == 0:
  print "No need to send jobs"
else:
  if not Test: print str(counter)+" jobs will be sent"
  else: print str(counter)+" jobs need to be sent"
