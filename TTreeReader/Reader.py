###########################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    17/09/2020
## Purpose: Read TTrees and fill histograms
###########################################################################################

# Jet matching and isolation requirements
Rmatch        = 0.3       # matching parameter
RIsoT         = 0.4 * 2.5 # reco-truth isolation requirement
RIsoR         = 0.4 * 1.5 # reco-reco isolation requirement
RecoIsopTCut  = 7         # min pt requirement to discard non-isolated reco jet
TruthIsopTCut = 7         # min pt requirement to discard non-isolated truth jet

UseEt = False # if False, pt is used

#Think about these chains:
#        ChainProp(name='HLT_3j200_pf_ftf_L1J100',         groups=MultiJetGroup + PrimaryLegGroup),
#        ChainProp(name='HLT_5j70_pf_ftf_0eta240_L14J15',  groups=MultiJetGroup + PrimaryLegGroup),
#        ChainProp(name='HLT_6j55_pf_ftf_0eta240_L14J15',  groups=MultiJetGroup + PrimaryLegGroup),

###########################################################################################
# DO NOT MODIFY (below this line)                                                         #
###########################################################################################

def extractThresholdMultiplicity(string):
  multiplicity = int(string.split('j')[0])
  threshold    = int(string.split('j')[1])
  return threshold,multiplicity

from ROOT import *
import os,sys,argparse
from HelperClasses import *
from HelperFunctions import *
from Arrays import *
from metadata_dict import MetadataFiles
from math import *
#import numpy as np

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--path',            action='store',      dest="path")
parser.add_argument('--outPATH',         action='store',      dest="outPATH")
parser.add_argument('--file',            action='store',      dest="inputFile")
parser.add_argument('--dataset',         action='store',      dest="dataset")
parser.add_argument('--debug',           action='store_true', dest="debug",   default=False)

args = parser.parse_args()

# Sample type
Dataset  = args.dataset

# Debugging
Debug = args.debug

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(1)
if args.path is None:
  print "ERROR: path to input file not provided, exiting"
  sys.exit(1)
if args.outPATH is None:
  print "ERROR: path to output files (outPATH) not provided, exiting"
  sys.exit(1)
if args.inputFile is None:
  print "ERROR: input file not provided, exiting"
  sys.exit(1)

#####################
# Output file name
#####################
inputFileName  = args.inputFile.replace(".root","")
OutName    = str(args.outPATH)
OutName   += Dataset
OutName   += "_" + inputFileName
if not UseEt:
  OutName += "_usePt"
if Debug:
  OutName += "_Debug"
OutName   += ".root"

#####################
# TTree name
#####################
TDirectoryName = "outTree"

# Max number of events for debuging
DebugMAXevents = 1000

#############################################
# Open TFile/TDirectory and loop over events
#############################################
print "INFO: Opening "+args.path+args.inputFile
tfile = TFile.Open(args.path+args.inputFile)
if not tfile:
  print "ERROR: "+args.path+args.inputFile+" not found, exiting"
  sys.exit(0)

tdir = TDirectoryFile()
tdir = tfile.Get(TDirectoryName)
if not tdir:
  print "ERROR: "+TDirectoryName+" not found, exiting"
  sys.exit(0)

# Get list of TTrees
Keys = tdir.GetListOfKeys()
keys = []
for key in Keys: keys.append(key.GetName())

# Set list of requested TTrees
keys = ['nominal']

# Loop over TTrees
for TTreeName in keys:

  # Open TFile
  print "INFO: Opening "+args.path+args.inputFile
  tfile = TFile.Open(args.path+args.inputFile)
  if not tfile:
    print "ERROR: "+args.path+args.inputFile+" not found, exiting"
    sys.exit(0)

  # Open corresponding directory
  tdir = TDirectoryFile()
  tdir = tfile.Get(TDirectoryName)
  if not tdir:
    print "ERROR: "+TDirectoryName+" not found, exiting"
    sys.exit(0)

  # Get TTree
  if Debug: print "DEBUG: Get "+TTreeName+" TTree"
  tree = tdir.Get(TTreeName)
  if not tree:
    print "ERROR: "+TTreeName+" not found, exiting"
    sys.exit(0)
  totalEvents = tree.GetEntries()
  print "INFO: TotalEvents in "+TTreeName+": "+str(totalEvents)

  ###################
  # Book histograms
  ###################

  # Central EM jet with highest ET vs central PF jet with highest ET
  name = 'EMet_vs_PFet' if UseEt else 'EMpt_vs_PFpt'
  Hist = TH2D(name,'',500,0,500,501,-1,500) # EM et(pt) vs PF(pt) et
  Hist.Sumw2()

  MJhists = dict()
  MJchains = ['4j115','5j85','6j70','7j45','10j40']
  for case in MJchains:
    name    = 'Njets_vs_EMEtThreshold__{}_PF'.format(case) if UseEt else 'Njets_vs_EMPtThreshold__{}_PF'.format(case)
    tmpHist = TH2D(name,'',130,0,130,20,0,20) # njets vs EM threshold
    tmpHist.Sumw2()
    MJhists[case] = tmpHist

  ################
  # Set branches
  ################
  Branches = dict()
  setBranchStatus(tree)
  setBranchAddress(tree,Branches)

  ###################
  # Loop over events
  ###################
  for event_counter in xrange(0,totalEvents):

    # Get entry
    tree.GetEntry(event_counter)

    event_counter += 1
    if Debug and event_counter > DebugMAXevents:
      break # skip loop
    if Debug:
      print "DEBUG: EventNumber: "+str(Branches["eventNumber"][0])
    if event_counter == 1:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
    if event_counter % 100000 == 0:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
 
    ####################
    # Get event weights
    ####################
    sampleWeight  = Branches["mcEventWeight"][0]
    wgt           = sampleWeight
    if Debug:
      print "DEBUG: mcEventWeight: "+str(Branches["mcEventWeight"][0])

    #################
    # Select PF jets
    #################
    SelectedPFJets = []
    if Debug: print('Select PF jets:')
    for ijet in xrange(0,getNPFJet(Branches)):
      jetPt  = getPFJetPt(Branches,ijet)
      jetEta = getPFJetEta(Branches,ijet)
      jetPhi = getPFJetPhi(Branches,ijet)
      jetE   = getPFJetE(Branches,ijet)
      # protection
      if jetPt < 0: continue # skip negative pt PF jet
      if abs(jetEta) >= 3.2: continue # skip forward PF jet
      TLVjet = iJet()
      TLVjet.SetPtEtaPhiE(jetPt,jetEta,jetPhi,jetE)
      if Debug and Calibrate: print('L1 jet kinematics before calibration')
      if Debug: print('PF jet pt: '+str(jetPt))
      if Debug: print('PF jet eta: '+str(jetEta))
      if Debug: print('PF jet phi: '+str(jetPhi))
      if Debug: print('PF jet E: '+str(jetE))
      SelectedPFJets.append(TLVjet)

    ####################
    # Select EM jets
    ####################
    SelectedEMJets = []
    if Debug: print('Select EM jets:')
    for ijet in xrange(0,getNEMJet(Branches)):
      jetPt  = getEMJetPt(Branches,ijet)
      jetEta = getEMJetEta(Branches,ijet)
      jetPhi = getEMJetPhi(Branches,ijet)
      jetE   = getEMJetE(Branches,ijet)
      # protection
      if jetE < 0: continue # skip negative energy EM jet
      if abs(jetEta) >= 3.2: continue # skip forward EM jet
      TLVjet = iJet()
      TLVjet.SetPtEtaPhiE(jetPt,jetEta,jetPhi,jetE)
      if Debug: print('Truth jet pt: '+str(jetPt))
      if Debug: print('Truth jet Et: '+str(TLVjet.Et()))
      if Debug: print('Truth jet eta: '+str(jetEta))
      if Debug: print('Truth jet phi: '+str(jetPhi))
      SelectedEMJets.append(TLVjet)

    ###################
    # Event selections
    ###################
    
    # At least one PF jet
    nPFJets    = len(SelectedPFJets)
    if nPFJets < 1: continue # skip event
    
    nEMJets = len(SelectedEMJets)

    ######################################
    # Fill 2D distribution
    ######################################

    if Debug:
      if UseEt: print('DEBUG: Leading PF jet ET = '+str(SelectedPFJets[0].Et()))
      else:     print('DEBUG: Leading PF jet pt = '+str(SelectedPFJets[0].Pt()))
    if nEMJets > 0:
      if UseEt: Hist.Fill(SelectedEMJets[0].Et(),SelectedPFJets[0].Et(),wgt)
      else:     Hist.Fill(SelectedEMJets[0].Pt(),SelectedPFJets[0].Pt(),wgt)
    else: # no central EM jet in this event
      if UseEt: Hist.Fill(-1,SelectedPFJets[0].Et(),wgt) # this event would not pass selection
      else:     Hist.Fill(-1,SelectedPFJets[0].Pt(),wgt) # this event would not pass selection

    # ['4j115','5j85','6j70','7j45','']
    for case in MJchains:
      PFthreshold,PFmultiplicity = extractThresholdMultiplicity(case)
      # Loop over PF jets
      counter = 0
      for PFjet in SelectedPFJets:
        value = PFjet.Et() if UseEt else PFjet.Pt()
	if value >= PFthreshold:
          counter += 1
      if counter >= PFmultiplicity:
        # Fill distribution
	for threshold in xrange(0,131):
          # Count number of jets above threhsold
	  counter = 0
	  for EMjet in SelectedEMJets:
            value = EMjet.Et() if UseEt else EMjet.Pt()
	    if value >= threshold:
              counter += 1
          MJhists[case].Fill(threshold,counter,wgt)

  ##################################
  # Write histograms to a ROOT file
  ##################################
  outFile = TFile(OutName,"UPDATE")
  Hist.Write()
  for key,hist in MJhists.items():
    hist.Write()
  outFile.Close()
  print "Histograms saved to "+OutName
print ">>> DONE <<<"
