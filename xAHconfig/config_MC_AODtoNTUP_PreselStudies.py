import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

msgLevel = "info"
#msgLevel = "debug"

# Event-level selections (GRL, trigger, etc)
c.algorithm("BasicEventSelection", { 
  "m_name"                    : "BasicSelection",
  "m_msgLevel"                : msgLevel,
  "m_forceData"               : False,
  "m_applyGRLCut"             : False, # Temporary (if I enable it I get no events passing GRL)
  "m_GRLxml"                  : "",
  "m_derivationName"          : "",  # Doesn't matter for xAODs
  "m_useMetaData"             : False, # Temporary
  "m_storePassHLT"            : True,
  "m_storeTrigDecisions"      : True,
  "m_applyTriggerCut"         : True,
  "m_triggerSelection"        : "(HLT|L1)_[1-9]?0?(j|J|mu|ht|mb)[0-9]+.*",
  "m_checkDuplicatesMC"       : True,
  "m_applyEventCleaningCut"   : True,
  "m_applyPrimaryVertexCut"   : False, # Temporary
  "m_doPUreweighting"         : False,
  # Add event-based jet cleaning?
  } )

# HLT jet collections to be saved
JetColls = [
  'HLT_AntiKt4EMTopoJets_subjesIS',
  'HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf',
]

# Branch name for each of the above HLT jet collections
JetNames = [
  'trigEM',
  'trigPF',
]

# Information to be saved for each of the above HLT jet collections
JetDetails = [
  'kinematic trackPV trackAll detectorEta',
  'kinematic trackPV trackAll detectorEta',
]

trigJetContainerName = " ".join(JetColls)
trigJetBranchName    = " ".join(JetNames)
trigJetDetailStr     = "|".join(JetDetails)

c.algorithm("TreeAlgo",   {
  "m_name"                    : "outTree",
  "m_msgLevel"                : msgLevel,

  # event and trigger
  "m_evtDetailStr"            : "pileup",
  "m_trigDetailStr"           : "basic passTriggers",
 
  # trigger jets
  "m_trigJetContainerName"    : trigJetContainerName,
  "m_trigJetBranchName"       : trigJetBranchName,
  "m_trigJetDetailStr"        : trigJetDetailStr,
} )


