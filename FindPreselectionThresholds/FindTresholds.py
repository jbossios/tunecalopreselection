Samples = [
  'ttbarwoPileup',
  'ttbarwPileup',
]

PFthresholds = [
  15,
  25,
  35,
  45,
  60,
  85,
  110,
  175,
  260,
  360,
  380,
  400,
  420,
  440,
  450,
  460,
  480,
  500,
  520,
]

##################################
# DO NOT MODIFY (below this line)
##################################

import sys,os,ROOT

def extractThresholdMultiplicity(string):
  multiplicity = int(string.split('j')[0])
  threshold    = int(string.split('j')[1])
  return threshold,multiplicity

ROOT.gROOT.SetBatch(True)  # so does not pop up plots!
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle/AtlasStyle.C")

# Global style settings
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetCanvasColor(-1)
ROOT.gStyle.SetPadColor(-1)
ROOT.gStyle.SetFrameFillColor(-1)
ROOT.gStyle.SetHistFillColor(-1)
ROOT.gStyle.SetTitleFillColor(-1)
ROOT.gStyle.SetFillColor(-1)
ROOT.gStyle.SetFillStyle(4000)
ROOT.gStyle.SetStatStyle(0)
ROOT.gStyle.SetTitleStyle(0)
ROOT.gStyle.SetCanvasBorderSize(0)
ROOT.gStyle.SetFrameBorderSize(0)
ROOT.gStyle.SetStatBorderSize(0)
ROOT.gStyle.SetTitleBorderSize(0)
ROOT.gStyle.SetLegendBorderSize(0)

ROOT.SetAtlasStyle()

InputPath = '/afs/cern.ch/user/j/jbossios/work/public/JetTrigger/DerivePreselections/Reader/HISTfiles/usePt/v1/'

MJchains = ['4j115','5j85','6j70','7j45','10j40']

# Loop over samples
ChosenThresholds = dict()
for sample in Samples:
  # Open input file
  InputFileName = InputPath
  if 'wo' in sample:
    InputFileName += 'ttbarwoPileup_user.jbossios.25671326._000001.tree_usePt.root'
  else:
    InputFileName += 'ttbarwPileup_user.jbossios.25671338._000001.tree_usePt.root'
  InputFile = ROOT.TFile.Open(InputFileName)
  if not InputFile:
    print('ERROR: {} not found, exiting'.format(InputFile))
    sys.exit(0)
  # Get histogram
  HistName = 'EMpt_vs_PFpt'
  Hist     = InputFile.Get(HistName)
  if not Hist:
    print('ERROR: {} not found in {}'.format(HistName,InputFileName))
  # Plot EM ET distribution for each PF threshold
  for threshold in PFthresholds:
    Canvas = ROOT.TCanvas()
    Projection = Hist.ProjectionY('{}_{}'.format(HistName,threshold),Hist.GetXaxis().FindBin(threshold),Hist.GetXaxis().FindBin(threshold))
    Projection.Draw()
    Projection.GetXaxis().SetTitle('EM jet  #it{p}_{T} [GeV]')
    Projection.GetYaxis().SetTitle('#events')
    if threshold == 45:
      Projection.GetXaxis().SetRangeUser(0,100)
    elif threshold == 60:
      Projection.GetXaxis().SetRangeUser(0,120)
    elif threshold == 110:
      Projection.GetXaxis().SetRangeUser(40,200)
    elif threshold == 175:
      Projection.GetXaxis().SetRangeUser(100,240)
    Canvas.SaveAs('Plots/{}_{}_{}.pdf'.format(sample,HistName,threshold))
  # Find EM threshold for each PF chain for which we always have enough njets
  for case in MJchains:
    PFthreshold,Multiplicity = extractThresholdMultiplicity(case)
    # Get Histogram
    name = 'Njets_vs_EMPtThreshold__{}_PF'.format(case)
    Hist = InputFile.Get(name)
    # Loop over EM threshold and get njets distribution
    thresholdFound = False
    for threshold in xrange(0,131):
      if thresholdFound: break
      Canvas = ROOT.TCanvas()
      LowerEdge  = Hist.GetXaxis().FindBin(threshold)
      Projection = Hist.ProjectionY(name+'_proj_{}'.format(threshold),LowerEdge,LowerEdge,'e')
      Projection.Draw()
      Projection.GetXaxis().SetTitle('Njets')
      Projection.GetYaxis().SetTitle('#events')
      Canvas.SaveAs('Plots/{}_{}.pdf'.format(sample,name+'_proj_{}'.format(threshold)))
      for ibin in xrange(1,Multiplicity+1):
        if Projection.GetBinContent(ibin) > 0:
          ChosenThreshold = threshold-1
	  thresholdFound  = True
          break
    #print('EM threshold for {} = {}'.format(case,ChosenThreshold))
    ChosenThresholds[sample+'_'+case] = ChosenThreshold

for key,threshold in ChosenThresholds.items():
  print('EM threshold for {} = {}'.format(key,threshold))
print('>>> ALL DONE <<<')
